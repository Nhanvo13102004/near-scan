import Image from 'next/image'
import { Inter } from 'next/font/google'
import Header from '@/components/header'
import HeroSelection from '@/components/heroSelection'
import Container from '@/components/container'
import Footer from '@/components/footer'


const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <main>
      <Header/>
      <HeroSelection className='relative'/>
      <Container/>
      <Footer/>
    </main>
  )
}
