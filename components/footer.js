import Image from "next/image"
import coin from '../public/coins.png'
import twitterIcon from "../public/twitter-icon.png"

export default function Footer(){
    return(
        <div className="bg-bottom-right">
            <div className="bg-bottom-left">
                <div className="container mx-auto px-3 pb-32">
                    <div className="grid grid-cols-1 lg:grid-cols-6 gap-5 py-5">
                        <div className="w-64 ml-5">
                            <div class="text-sm text-grey-dark flex flex-row py-3">
                                <Image className="h-10 w-10" src={coin} alt='Near'/>
                                <p className="text-2xl ml mt-1 font-semibold">Powered by NEAR</p>
                            </div>
                            <p class="max-w-xs text-black text-xs leading-6 pb-6">NearExplorer is a Blockchain Explorer and Analytics Platform for Near Protocol, a new blockchain and smart transaction platform.</p>
                            <div class="hidden lg:block"></div>
                        </div>
                        <div class="absolute right-10">
                            <div class="text-[#16a085] mt-3 font-semibold text-2xl mb-3">Explore</div>
                            <ul class="text-black opacity-80 footer-links text-sm leading-6">
                                <li>
                                    <a target="_blank" rel="noreferrer nofollow noopener" href="#">Latest Blocks</a>
                                </li>
                                <li>
                                    <a class="flex" target="_blank" rel="noreferrer nofollow noopener" href="#">Latest Transactions
                                    </a>
                                </li>
                                <li>
                                    <a class="flex" target="_blank" rel="noreferrer nofollow noopener" href="#">Charts & Stats
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="flex justify-between border-t border-[#b7bfc9]">
                        <p class=" text-xs py-4 text-center ">NearExplorer © 2023 by
                            <a href="#" target="_blank" class="font-semibold text-[#152347]" rel="noreferrer nofollow noopener"> Huu Nhan</a>
                        </p>
                        <div class="pt-3 mr-5">
                            <a href="https://twitter.com/_huu_nhnz04" target="_blank" rel="noreferrer nofollow noopener">
                                <Image priority height={32} width={32} src={twitterIcon} alt="Twttiter" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}