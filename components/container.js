export default function Container(){
    return(
        <section>
            <div className="container mx-auto px-3 z-10 mt-10">
                <div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
                    <div className="w-full">
                        <div className="bg-white soft-shadow rounded-lg overflow-hidden mb-6 md:mb-10">
                            <h2 class="border-b p-3 text-gray-600 text-sm font-semibold">Latest Blocks</h2>
                            <div class="relative">
                                <div class="scrollbar-container ps ps--active-y">
                                    <div class="px-3 divide-y h-80">
                                        <div class="grid grid-cols-2 md:grid-cols-3 gap-2 lg:gap-3 py-3">
                                            <div class=" flex items-center">
                                                <div class="flex-shrink-0 rounded-lg h-10 w-10 bg-blue-900/10 flex items-center justify-center text-sm">BK</div>
                                                <div class="overflow-hidden pl-2">
                                                    <div class="text-green-500 text-sm font-medium ">
                                                        <a class="text-green-500" href="#">98,149,149</a>
                                                    </div>
                                                    <div class="text-gray-400 text-xs truncate">a few seconds ago</div>
                                                </div>
                                            </div>
                                            <div class="col-span-2 md:col-span-1 px-2 order-2 md:order-1 text-sm whitespace-nowrap truncate">
                                                Author:  
                                                <a class="text-green-500 font-medium" href="#"> hb436_pool.poolv1.near</a>
                                                <div class="text-gray-400 text-sm ">2 txns </div>
                                            </div>
                                            <div class="text-right order-1 md:order-2 overflow-hidden">
                                                <span class="text-sm border px-1 py-0.5 rounded border-[#00b894] text-gray-400 truncate" data-state="tooltip-hidden" data-reach-tooltip-trigger="">58.9 Tgas</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="border-t px-2 py-3 text-gray-700">
                                <a class="block text-center border border-green-900/10 bg-[#00b894] hover:bg-[#00b893aa] text-white text-base py-3 rounded w-full focus:outline-none" href="#">View all blocks</a>
                            </div>
                        </div>
                    </div>
                    <div className="w-full">
                        <div className="bg-white soft-shadow rounded-lg overflow-hidden mb-6 md:mb-10">
                            <h2 class="border-b p-3 text-gray-500 text-sm font-semibold">Latest Transactions</h2>
                            <div class="relative">
                                <div class="scrollbar-container ps ps--active-y">
                                    <div class="px-3 divide-y h-80">
                                        <div class="grid grid-cols-2 md:grid-cols-3 gap-3 lg:gap-3 items-center py-3">
                                            <div class=" flex items-center">
                                                <div class="flex-shrink-0 rounded-full h-10 w-10 bg-blue-900/10 flex items-center justify-center text-sm">TX</div>
                                                <div class="overflow-hidden pl-2">
                                                    <div class="text-green-500 text-sm ">
                                                        <a class="text-green-500 font-medium" href="#">3Zketh...QeYq</a>
                                                    </div>
                                                    <div class="text-gray-400 text-xs truncate">a few seconds ago</div>
                                                </div>
                                                
                                            </div>
                                            <div class="col-span-2 md:col-span-1 px-2 order-2 md:order-1 text-sm">
                                                    <div class="whitespace-nowrap truncate">
                                                        From: 
                                                        <a class="text-green-500 font-medium" href="#"> 6a0161a85a...64b3138</a>
                                                    </div>
                                                    <div class="whitespace-nowrap truncate">
                                                    To: 
                                                    <a class="text-green-500 font-medium" href="#"> embr.playe...ve.near</a>
                                                    </div>
                                            </div>
                                            <div class="text-right order-1 md:order-2 overflow-hidden">
                                                    <span class="text-sm border px-2 rounded border-[#00b894] text-gray-400 truncate" data-state="tooltip-hidden" data-reach-tooltip-trigger="">0 Ⓝ</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="border-t px-2 py-3 text-gray-700">
                                <a class="block text-center border border-green-900/10 bg-[#00b894] hover:bg-[#00b893aa] text-white text-base py-3 rounded w-full focus:outline-none" href="#">View all blocks</a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    )
}