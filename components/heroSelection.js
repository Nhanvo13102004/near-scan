import Image from "next/image"
import coin from '../public/coins.png'

export default function HeroSelection(){
    return(
        <div>
            <section class="bg-white dark:bg-gray-900">
                <div class="grid max-w-screen-xl px-8 lg:gap-8 lg:py-10 lg:grid-cols-12">
                    <div class="mr-auto place-self-center lg:col-span-7">
                        <h1 class="max-w-2xl mb-4 text-2xl font-extrabold tracking-tight leading-none md:text-2xl dark:text-white">Near Protocol Explorer</h1>
                        <p class="max-w-4xl mb-6 font-light text-gray-500 lg:mb-8 md:text-sm lg:text-lg dark:text-gray-400">Sponsored:Stader Labs - Get 50%+ APY & Assurance of Fund Safety with NearX Multi-Layer Security | Now Integrated with Burrow | NearX - Live on Aurora & NEAR.</p>
                    </div>             
                </div>
            </section>
            <div className="relative -mt-14">
                <div className="container mx-auto px-3">
                    <div className="bg-white h-[180px] soft-shadow rounded-lg overflow-hidden px-5 md:py lg:px-0">
                        <div className="grid grid-flow-col grid-cols-1 grid-rows-3 lg:grid-cols-3 lg:grid-rows-1 divide-y lg:divide-y-0 lg:divide-x lg:py-3">
                            <div class="flex flex-col lg:flex-col lg:items-stretch divide-y lg:divide-y lg:divide-x-0 md:pt-0 md:pb-0 md:px-5">
                                <div class="flex flex-row py-5 lg:pb-5 lg:px-0">
                                    <div class="items-center flex justify-left mr-3 ">
                                        <Image alt="Icon" src={coin} className="h-8 w-8" />
                                    </div>
                                    <div class="ml">
                                        <p class="uppercase font-semibold text-gray-600 text-sm ">Near price</p>
                                        <a class="leading-6 text-gray-500" href="/charts/near-price">$1.35 
                                            <span class="text-gray-400">@0.00005 BTC</span>
                                            <span class="text-neargreen text-sm">(0.50%)</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="flex flex-row py-5 lg:pt-5 lg:px-0">
                                    <div class="items-center flex justify-left mr-3 ">
                                        <svg width="30" height="30" viewBox="0 0 66 66" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M33 66C14.7963 66 0 51.2037 0 33C0 14.7963 14.7963 0 33 0C51.2037 0 66 14.7963 66 33C66 51.2037 51.2037 66 33 66ZM33 4.60465C17.3442 4.60465 4.60465 17.3442 4.60465 33C4.60465 48.6558 17.3442 61.3953 33 61.3953C48.6558 61.3953 61.3953 48.6558 61.3953 33C61.3953 17.3442 48.6558 4.60465 33 4.60465Z" fill="#292D32"/>
                                            <path d="M23.7915 62.9302H20.7217C19.4631 62.9302 18.4194 61.8865 18.4194 60.6279C18.4194 59.3693 19.4017 58.3563 20.6603 58.3256C15.8408 41.8716 15.8408 24.1284 20.6603 7.67441C19.4017 7.64372 18.4194 6.63069 18.4194 5.37209C18.4194 4.11348 19.4631 3.06976 20.7217 3.06976H23.7915C24.5282 3.06976 25.2343 3.43814 25.664 4.02139C26.0938 4.63534 26.2166 5.40279 25.971 6.10883C20.1998 23.453 20.1998 42.547 25.971 59.9219C26.2166 60.6279 26.0938 61.3953 25.664 62.0093C25.2343 62.5619 24.5282 62.9302 23.7915 62.9302Z" fill="#292D32"/>
                                            <path d="M42.2105 62.9308C41.965 62.9308 41.7194 62.9001 41.4738 62.808C40.2766 62.409 39.6012 61.089 40.031 59.8918C45.8022 42.5476 45.8022 23.4536 40.031 6.07869C39.6319 4.88148 40.2766 3.56148 41.4738 3.16241C42.7017 2.76334 43.991 3.40799 44.3901 4.6052C50.4989 22.901 50.4989 43.0387 44.3901 61.3039C44.0831 62.3169 43.1622 62.9308 42.2105 62.9308Z" fill="#292D32"/>
                                            <path d="M32.9999 48.9622C24.4353 48.9622 15.9013 47.765 7.67435 45.3398C7.64366 46.5677 6.63063 47.5808 5.37203 47.5808C4.11342 47.5808 3.0697 46.537 3.0697 45.2784V42.2087C3.0697 41.4719 3.43807 40.7659 4.02133 40.3361C4.63528 39.9064 5.40273 39.7836 6.10877 40.0291C23.453 45.8003 42.5776 45.8003 59.9218 40.0291C60.6278 39.7836 61.3953 39.9064 62.0092 40.3361C62.6232 40.7659 62.9609 41.4719 62.9609 42.2087V45.2784C62.9609 46.537 61.9171 47.5808 60.6585 47.5808C59.3999 47.5808 58.3869 46.5984 58.3562 45.3398C50.0985 47.765 41.5646 48.9622 32.9999 48.9622Z" fill="#292D32"/>
                                            <path d="M60.6273 26.0926C60.3817 26.0926 60.1361 26.0619 59.8905 25.9698C42.5463 20.1986 23.4216 20.1986 6.07744 25.9698C4.84953 26.3689 3.56023 25.7242 3.16116 24.527C2.79279 23.2991 3.43744 22.0098 4.63465 21.6107C22.9305 15.5019 43.0682 15.5019 61.3333 21.6107C62.5305 22.0098 63.2059 23.3298 62.7761 24.527C62.4998 25.4786 61.5789 26.0926 60.6273 26.0926Z" fill="#292D32"/>
                                        </svg>

                                    </div>
                                    <div class="ml">
                                        <p class="uppercase font-semibold text-gray-600 text-sm">Market cap</p>
                                        <a class="leading-6 text-gray-400" href="#">$1,272,021,430.00</a>
                                    </div>
                                </div>
                            </div>
                            <div class="flex flex-col lg:flex-col lg:items-stretch divide-y lg:divide-y lg:divide-x-0 md:pt-0 md:pb-0 md:px-5">
                                    <div class="flex flex-row justify-between py-5 lg:pb-5 lg:px-0">
                                        <div class="flex flex-row ">
                                            <div class="items-center flex justify-left mr-3 ">
                                                <svg width="30" height="30" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M52.4983 3C62.3979 3 68.9977 9.59977 68.9977 19.4994V26.0992" stroke="#292D32" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M3 26.0992V19.4994C3 9.59977 9.59977 3 19.4994 3H38.5398" stroke="#292D32" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M69 45.8985V52.4983C69 62.3979 62.4002 68.9977 52.5006 68.9977H34.2522" stroke="#292D32" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M3 45.8985V52.4983C3 62.3979 9.59977 68.9977 19.4994 68.9977" stroke="#292D32" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M18.5069 26.9572L35.9962 37.0878L53.3536 27.0232" stroke="#292D32" stroke-width="5" stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M35.9988 55.0395V37.0551" stroke="#292D32" stroke-width="5" stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M31.9077 17.1567L21.3482 23.0306C18.9722 24.3505 16.9923 27.6834 16.9923 30.4224V41.609C16.9923 44.3479 18.9392 47.6807 21.3482 49.0007L31.9077 54.8745C34.1516 56.1285 37.8475 56.1285 40.1244 54.8745L50.684 49.0007C53.0599 47.6807 55.0399 44.3479 55.0399 41.609V30.4224C55.0399 27.6834 53.0929 24.3505 50.684 23.0306L40.1244 17.1567C37.8475 15.8697 34.1516 15.8697 31.9077 17.1567Z" stroke="#292D32" stroke-width="5" stroke-linecap="round" stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                            <div class="ml">
                                                <p class="uppercase font-semibold text-gray-600 text-sm">Transactions</p>
                                                <p class="leading-6 text-gray-400">339.47 M</p>
                                            </div>
                                        </div>
                                        <div class="flex flex-col text-right">
                                            <p class="uppercase font-semibold text-gray-600 text-sm"> GAS price</p>
                                            <p class="leading-6 text-gray-400">0.0001 Ⓝ / Tgas</p>
                                        </div>
                                    </div>
                                    <div class="flex flex-row justify-between align-center py-5 lg:pt-5 lg:px-0">
                                        <div class="flex flex-row ">
                                            <div class="items-center flex justify-left mr-3 ">
                                                <svg width="30" height="30" viewBox="0 0 66 66" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M33 66C14.7963 66 0 51.2037 0 33C0 14.7963 14.7963 0 33 0C51.2037 0 66 14.7963 66 33C66 51.2037 51.2037 66 33 66ZM33 4.60465C17.3442 4.60465 4.60465 17.3442 4.60465 33C4.60465 48.6558 17.3442 61.3953 33 61.3953C48.6558 61.3953 61.3953 48.6558 61.3953 33C61.3953 17.3442 48.6558 4.60465 33 4.60465Z" fill="#292D32"/>
                                                    <path d="M44.3877 45.0642C43.9886 45.0642 43.5895 44.9721 43.2211 44.7265L33.7049 39.0474C31.3411 37.6353 29.5914 34.5349 29.5914 31.8028V19.2168C29.5914 17.9582 30.6351 16.9144 31.8937 16.9144C33.1523 16.9144 34.196 17.9582 34.196 19.2168V31.8028C34.196 32.9079 35.117 34.5349 36.0686 35.0874L45.5849 40.7665C46.69 41.4112 47.0276 42.8232 46.383 43.9284C45.9225 44.6651 45.1551 45.0642 44.3877 45.0642Z" fill="#292D32"/>
                                                </svg>
                                            </div>
                                            <div class="ml">
                                                <p class="uppercase font-semibold text-gray-600 text-sm">Active validators</p>
                                                <a class="leading-6 text-gray-400" href="/charts/blocks">215</a>
                                            </div>
                                        </div>
                                        <div class="flex flex-col text-right">
                                            <p class="uppercase font-semibold text-gray-600 text-sm">AVG. BLOCK TIME</p>
                                            <a class="leading-6 text-gray-400">1.36360000 s</a>
                                        </div>
                                    </div>
                            </div>
                            <div className="md:col-span-2 lg:col-span-1 flex flex-col lg:flex-col lg:items-stretch divide-y lg:divide-y lg:divide-x-0 md:pt-0 md:px-5">
                                    <div className="flex-1 py-5 lg:px-0">
                                        <p class="uppercase font-semibold text-gray-600 text-sm"> Near transaction history in 14 days</p>
                                        <div data-highcharts-chart="0">
                                            <div id="highcharts-8lngrql-0" dir="ltr" class="highcharts-container">
                                            </div>
                                        </div>
                                    </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}