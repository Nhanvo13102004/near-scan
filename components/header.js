import Image from 'next/image';
import Logo from '../public/logo_near.svg'


export default function Header(){
    return(
        <header className='sticky top-0 border-b border-gray-300'>
            <nav class="bg-white border-gray-200 px-4 lg:px-6 py-2.5 ">
                <div class="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl">
                    <a href="https://near.org" class="flex justify-start items-center">
                        <Image priority src={Logo} class="mr-3" alt="Flowbite Logo" />
                    </a>
                    <form className="flex w-2/5">
                            <button id="dropdown-button" data-dropdown-toggle="dropdown" className="flex-shrink-0 z-10 inline-flex items-center py-1 px-4 text-sm font-medium text-center text-gray-900 bg-gray-100 border border-gray-300 rounded-l-lg hover:bg-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100" type="button">All Filters <svg class="w-2.5 h-2.5 ml-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 4 4 4-4"/>
                                </svg>
                            </button>
                            <div id="dropdown" className="z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-44">
                                <ul class="py-2 text-sm text-black " aria-labelledby="dropdown-button">
                                    <li>
                                        <button type="button" className="inline-flex w-full px-4 py-2 hover:bg-gray-100">Address</button>
                                    </li>
                                    <li>
                                        <button type="button" className="inline-flex w-full px-4 py-2 hover:bg-gray-100">Token</button>
                                    </li>
                                    <li>
                                        <button type="button" className="inline-flex w-full px-4 py-2 hover:bg-gray-100">Name Tags</button>
                                    </li>
                                    <li>
                                        <button type="button" className="inline-flex w-full px-4 py-2 hover:bg-gray-100">Domain Name</button>
                                    </li>
                                </ul>
                            </div>
                            <div class="relative w-full">
                                <input type="search" id="search-dropdown" className="block p-1 w-full z-20 text-sm text-gray-900 outline-none bg-gray-50 rounded-r-lg border-l-gray-50 border-l-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search by Address / Txn Hash / Block / Token" required/>
                                <button type="submit" className="absolute top-0 right-0 p-1 text-sm font-medium h-full text-white bg-blue-700 rounded-r-lg border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300">
                                    <svg class="w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                                    </svg>
                                    <span class="sr-only">Search</span>
                                </button>
                            </div>
                    </form>
                    {/* <div class="flex items-center lg:order-2">
                        <button data-collapse-toggle="mobile-menu-2" type="button" class="inline-flex items-center p-2 ml-1 text-sm text-gray-500 rounded-lg lg:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="mobile-menu-2" aria-expanded="false">
                            <span class="sr-only">Open main menu</span>
                            <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
                            <svg class="hidden w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        </button>
                    </div> */}
                    <div class="hidden justify-between items-center w-full lg:flex lg:w-auto lg:order-1" id="mobile-menu-2">
                        <ul class="flex flex-col mt-4 mr-3 font-medium lg:flex-row lg:space-x-8 lg:mt-0">
                            <li>
                                <a href="#" class="block py-2 pr-4 pl-3 text-[#3498db] rounded bg-primary-700 lg:bg-transparent lg:text-primary-700 lg:p-0" aria-current="page">Home</a>
                            </li>
                            <li class="flex flex-row group cursor-pointer py-2 pr-4 pl-3 text-black border-b lg:hover:bg-transparent lg:border-0 lg:hover:text-primary-700 lg:p-0">
                                <button className='flex flex-row hover:text-[#3498db] focus:text-[#3498db]' >Blockchain
                                <svg class="w-2.5 h-2.5 ml-1.5 mt-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 4 4 4-4"/>
                                </svg>
                                </button>
                                <div className='absolute top-[0] left-[75%]  transition group-hover:translate-y-7 translate-y-0 invisible opacity-0 
                                group-hover:opacity-100 group-hover:visible duration-200 ease-in-out group-hover:transform z-50 min-w-[150px] transform'>
                                    <div className='relative top-5 p-2 bg-white rounded-b-lg border-t-4 border-[#3498db] shadow-xl w-full'>
                                        <p className='bg-white hover:bg-[#e0e0e0] p-1 px-3 text-sm text-[#484848] rounded'>View Blocks</p>
                                        <p className='bg-white hover:bg-[#e0e0e0] p-1 px-3 text-sm text-[#484848] rounded'>View Txns</p>
                                        <p className='bg-white hover:bg-[#e0e0e0] p-1 px-3 text-sm text-[#484848] rounded'>Charts & Stats</p>
                                    </div>
                                </div>
                            </li>
                            <li class="flex flex-row group cursor-pointer py-2 pr-4 pl-3 text-black border-b hover:text-[#3498db] lg:hover:bg-transparent lg:border-0 lg:hover:text-primary-700 lg:p-0">
                                <button className='flex flex-row hover:text-[#3498db] focus:text-[#3498db]' >Tokens
                                    <svg class="w-2.5 h-2.5 ml-1.5 mt-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 4 4 4-4"/>
                                    </svg>
                                </button>
                                <div className='absolute top-[0] left-[83%]  transition group-hover:translate-y-7 translate-y-0 invisible opacity-0 
                                group-hover:opacity-100 group-hover:visible duration-200 ease-in-out group-hover:transform z-50 min-w-[150px] transform'>
                                    <div className='relative top-5 p-2 bg-white rounded-b-lg border-t-4 border-[#3498db] shadow-xl w-full'>
                                        <p className='bg-white hover:bg-[#e0e0e0] p-1 px-3 text-sm text-[#484848] rounded'>Top Tokens</p>
                                        <p className='bg-white hover:bg-[#e0e0e0] p-1 px-3 text-sm text-[#484848] rounded'>Token Transfers</p>
                                        <p className='bg-white hover:bg-[#e0e0e0] p-1 px-3 text-sm text-[#484848] rounded'>Top NFT Tokens</p>
                                        <p className='bg-white hover:bg-[#e0e0e0] p-1 px-3 text-sm text-[#484848] rounded'>NFT Token Transfers</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <span className="mr-2 ml-1">|</span>
                        <span class="inline-block h-5 w-5 bg-gray-500 rounded-full overflow-hidden mr-1.5 mt-1">
                            <svg class="h-full w-full text-gray-500" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.62854" y="0.359985" width="15" height="15" rx="7.5" fill="white"/>
                                <path d="M8.12421 7.20374C9.21151 7.20374 10.093 6.32229 10.093 5.23499C10.093 4.14767 9.21151 3.26624 8.12421 3.26624C7.0369 3.26624 6.15546 4.14767 6.15546 5.23499C6.15546 6.32229 7.0369 7.20374 8.12421 7.20374Z" fill="currentColor"/>
                                <path d="M11.818 10.5975C10.2992 12.6412 7.42106 13.0631 5.37731 11.5537C5.01171 11.2818 4.69296 10.9631 4.42107 10.5975C4.28982 10.4006 4.27107 10.1475 4.37419 9.94123L4.51482 9.65059C4.84296 8.95684 5.53671 8.51624 6.30546 8.51624H9.95231C10.7023 8.51624 11.3867 8.94749 11.7242 9.62249L11.8742 9.93184C11.968 10.1475 11.9586 10.4006 11.818 10.5975Z" fill="currentColor"/>
                            </svg>
                        </span>
                        <button type="submit" className="flex flex-col font-medium hover:text-[#3498db]">Sign In</button>
                    </div>
                </div>
            </nav>
        </header>
    )
}